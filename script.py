import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import pandas as pd
import mplcursors
import numpy as np
import operator

plt.style.use('seaborn-whitegrid')

# Import des données sur la ville inconnue
df = pd.read_excel(r'data/Climat.xlsx', "SI -erreur", usecols=range(3,15), skiprows=4, skipfooter=17, header=None)
# Ajout des noms de colonnes pour décrire les mois
months = [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre'

]
df.columns = months

print("------------- Température ville inconnue -------------")
print(df)
print(df.describe())

df.dropna()

#Traitement de correction
#Correction des valeurs != int ou float en faisant la moyenne entre le jour precedent et le jour suivant
for columnIndex in range(len(df.columns)):
    columnIndexPosition = columnIndex #-1
    column = df[df.columns[columnIndexPosition]]
    for index in range(len(column.dropna())):
        indexPosition = index#-1
        
        #Si pas beau
        
        if not (isinstance(column.iloc[indexPosition],np.float64) or isinstance(column.iloc[indexPosition],int)) :
            #Valeur veille
            indexPrevious = indexPosition
            indexPreviousMonth = columnIndexPosition
            lePrevious = 0
            while True:
                indexPrevious = indexPrevious-1
                if indexPrevious == -1 :
                    indexPreviousMonth = indexPreviousMonth-1
                    indexPrevious = len(df[df.columns[indexPreviousMonth]].dropna()) -1
         
                lePrevious = df[df.columns[indexPreviousMonth]][indexPrevious]
                if (isinstance(lePrevious,np.float64) or isinstance(lePrevious,int)):
                    break

            #Valeur lendemain
            indexNext = indexPosition+1
            indexNextMonth = columnIndexPosition
            leNext =0
            
            while True:
                indexNext = indexNext+1
                if indexNext == len(column.dropna()) :
                    indexNextMonth = indexNextMonth+1
                    indexNext = 0
                leNext = df[df.columns[indexNextMonth]][indexNext]
                if (isinstance(lePrevious,np.float64) or isinstance(lePrevious,int)):
                    break

            newValue = (lePrevious + leNext) / 2

            column[indexPosition]=newValue

#Correction des valeurs trop eloignées de la moyenne du mois
for columnIndex in range(len(df.columns)):
    columnIndexPosition = columnIndex #-1
    column = df[df.columns[columnIndexPosition]]
    moyDuMois = column.mean()
    for index in range(len(column.dropna())):
        indexPosition = index#-1
        
        #Si pas beau
        
        if ((column.iloc[indexPosition]-moyDuMois > 12) or (column.iloc[indexPosition]-moyDuMois < -12) ) :
            #Valeur veille
            indexPrevious = indexPosition
            indexPreviousMonth = columnIndexPosition
            lePrevious = 0
            while True:
                indexPrevious = indexPrevious-1
                if indexPrevious == -1 :
                    indexPreviousMonth = indexPreviousMonth-1
                    indexPrevious = len(df[df.columns[indexPreviousMonth]].dropna()) -1
         
                lePrevious = df[df.columns[indexPreviousMonth]][indexPrevious]
                if ((lePrevious-moyDuMois > 12) or (lePrevious-moyDuMois < -12) ):
                    break

            #Valeur lendemain
            indexNext = indexPosition+1
            indexNextMonth = columnIndexPosition
            leNext =0
            
            while True:
                indexNext = indexNext+1
                if indexNext == len(column.dropna()) :
                    indexNextMonth = indexNextMonth+1
                    indexNext = 0
                leNext = df[df.columns[indexNextMonth]][indexNext]
                if ((lePrevious-moyDuMois > 12) or (lePrevious-moyDuMois < -12) ):
                    break

            newValue = (lePrevious + leNext) / 2

            column[indexPosition]=newValue


# Fin correction
print(df)
print("------------- Moyenne -------------")
print(df.mean())
print("------------- Ecart type -------------")
print(df.std())
print("------------- Min par mois -------------")
minparmois = df.min()
print(minparmois)
print("------------- Max par mois -------------")
maxparmois = df.max()
print(maxparmois)
print("------------- Min par ans -------------")
print(minparmois.min())
print("------------- Max par ans -------------")
print(maxparmois.max())


# Affichage des diagrammes de températures pour chaques mois
for column in df:
    plt.plot(df[column])
    plt.title('Variation de la température sur le mois de ' + column)
    plt.xlabel('Jours')
    plt.ylabel('Température (°C)')
    plt.show()
    mplcursors.cursor(hover=True)

# Mise en commun des données de l'année
allyear = pd.DataFrame(columns=['Temperature'])
nbOfDays = 1
for column in df:
    for value in df[column].dropna():
        allyear.loc["J"+str(nbOfDays)] = [value]
        nbOfDays = nbOfDays +1
print("ALL YEAR")
print(allyear)

allyear.reset_index().plot(x='index', y='Temperature')
plt.title('Variation de la température sur l\'année')
plt.xlabel('Jours')
plt.ylabel('Température (°C)')
plt.gca().xaxis.set_minor_locator(MultipleLocator(5))
plt.tick_params(which='minor', length=4)
plt.gca().yaxis.set_ticks(range(-25, 25, 5))

mplcursors.cursor(hover=True)
plt.show()

# Récupération des données de température des villes du monde
wCities = pd.read_csv('data/city_temperature.csv', sep=',', low_memory=False)

# Selection des villes Européennes uniquement
eCities = wCities.loc[wCities['Region'] == 'Europe']
# Suppression des données de localisation (Europe uniquement)
# Nous allons garder uniquement la ville
eCities = eCities.drop(columns=['Region', 'Country', 'State'])
# Suppression des températures anormales
# (dans le fichier openData, des valeurs sont à -99°, et comme ces valeurs sont en farenheit
# ce n'est pas possible, il faut donc les supprimer)
eCities = eCities.loc[eCities['AvgTemperature'] != -99]
# L'année du fichier de référence étant 2018, nous appliqueront cette règle à ces données
# en ne gardant que l'année 2018
eCities = eCities.loc[eCities['Year'] == 2018]
# Conversion des températures de farenheit en celsius
eCities['AvgTemperature'] = (eCities['AvgTemperature'] - 32) * 5/9

# Création d'un objet pour mettre en relation les numéros de mois du fichier OpenDataEurope et les mois littéraux de la ville inconnue.
monthMap = {
    1: "Janvier",
    2: "Février",
    3: "Mars",
    4: "Avril",
    5: "Mai",
    6: "Juin",
    7: "Juillet",
    8: "Août",
    9: "Septembre",
    10: "Octobre",
    11: "Novembre",
    12: "Décembre"
}

findCity = ''
citiesDiff = {}
# On boucle sur lignes de chaques villes
for city in eCities['City'].unique():
    cityTemps = eCities.loc[eCities['City'] == city]
    tempDiff = 0
    # On boucle sur chaques mois
    for month in cityTemps['Month'].unique():
        # Pour chaque mois, on viens calculer la différence de température entre les deux 
        tempDiff += abs(float(cityTemps['AvgTemperature'].loc[cityTemps['Month'] == month].mean()) - float(df[monthMap.get(month)].mean()))
    citiesDiff[city] = tempDiff

# On sélectionne la ville dont la différence est la moins grande pour définir la ville inconnue
maxDif = 1000
for x in citiesDiff:
    if (citiesDiff[x] < maxDif):
        maxDif = citiesDiff[x]
        findCity = x


# Comparaison des scores pour les villes les plus pertinentes
print("Classement des villes pertinantes :")
scoreTable = sorted(citiesDiff.items(), key=operator.itemgetter(1))
print(list(scoreTable)[:5], sep = "\n")

print("\n")
print('La ville dont la température moyenne est la plus proche est : ' + findCity)
